    /* Springshare Chat - David Bass - 13 Sep 2013 --- */

    var libchat_btn = {
       iid: "92",
       key: "c08f3a912c49c73",
       domain: "askus.library.wwu.edu",
       splash: "Welcome to Western Libraries Chat!",
       button_online: "//library.wwu.edu/info/primo/entypo.svg#U+E720",
       button_online: "http://library.wwu.edu/info/primo/wwuchat_on4.png",
       button_offline: "http://library.wwu.edu/info/primo/wwuchat_off4.png",
       offline_url: "http://askus.library.wwu.edu/browse.php?tid=23567",
       question: "Please enter question below:",
       star_ratings: true,
       star_text: "Please rate this chat:",
       depart_id: "146"
    };


jQuery(document).ready(function() {
        jQuery.ajax({
            url : '//' + libchat_btn.domain + '/chat_client_status.php',
            dataType : "jsonp",
            data: { d: libchat_btn.depart_id, iid: libchat_btn.iid },
            success : function(data) {
                // data.status = false;
               // data.status = true;

                if (data.status) {
                    // chat
                    var chatUrl = "http://libanswers.com/chati.php?k=c08f3a912c49c73&w=1&d=146&i=92&r=http%3A%2F%2Fonesearch.library.wwu.edu%2Fprimo_library%2Flibweb%2Faction%2Fsearch.do%3Fvid%3DWWU&t=Welcome%20to%20Western%20Libraries%20Chat!&iq=Please%20enter%20question%20below%3A";
                    jQuery("#libchat_btn_widget").attr("href", chatUrl);
                    jQuery("#libchat_btn_widget").attr("target", "libchat");

                    var chatModalWindow = '<!-- popup for Chat with a Librarian--> ' +
                        '<div id="chatModal" >' +
                         ' <div class="chat0">' +
                         '   <div class="chat1">' +
                          '    <div class="chat2">' +
                          '      <button type="button" id="close-chat" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                           '     <h4 id="chat-with-a-librarian-h4">Questions? Chat with Us</h4>' +
                           '   </div>' +
                           '   <div class="chat-body">' +
                           '     <iframe id="chat-iframe" src="' + chatUrl + '"></iframe>' +
                            '      <div id="chat-note"> Please note: changing to a new page will end your chat  </div>    ' +
                           '   </div>' +
                           ' </div>' +
                          '</div>' +
                        '</div>';

                    jQuery("body").prepend(chatModalWindow);

                    var chatHtml = '<div id="chatDiv">  <a id="libchat_btn_widget2" ><div id="chatDivLinkContainer">  <div id="chatDivText">Questions?<br> Chat with Us.</div> <span  id="chat-with-a-librarian-icon" class="fa  fa-comments"></span></div>';

                    jQuery("#site-header").append(chatHtml);

                    jQuery(document).on("click","#chatDiv", function() {
                        jQuery("#chatModal").show();
                    });

                    jQuery(document).on("click","#close-chat", function() {
                        jQuery("#chatModal").hide();
                    });

                } else {
                    // email - in menu
                    jQuery("#libchat_btn_widget").attr("href", "http://libguides.wwu.edu/content.php?pid=60282&sid=443326");
                    jQuery("#libchat_btn_widget").attr("title", "email a librarian");
                    jQuery("#libchat_btn_widget").html("<span class='fa fa-envelope-o'></span> email a librarian");

                    var chatHtml = '<div id="chatDiv">  <div id="chatDivLinkContainer">  <div id="chatDivText"><a href="http://libguides.wwu.edu/content.php?pid=60282&sid=443326" target="_email">Questions?<br> Send us an Email.</div> <span  id="chat-with-a-librarian-icon" class="fa  fa-envelope"></span></a></div>';

                    jQuery("#exlidSearchBanner").html(chatHtml);
                    jQuery("#exlidSearchBanner").show();

                }
            }
        }); 
}); 
